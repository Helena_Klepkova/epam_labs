﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DAL.Interfaces;
using DAL.Repositories;
using DAL.Entities;
using BLL.Interfaces;


namespace BLL.AppData
{
    public class Data : IData
    {
       CityRepository cityRepository;
       OrderRepository orderRepository;
       TicketRepository ticketRepository;
       UserRepository userRepository;
       VenueRepository venueRepository;
       EventRepository eventRepository;
       PasswordReposirory passwordRepository;
       RoleRepository roleRepository;

        public Data()
        {
            cityRepository = new CityRepository();
            eventRepository = new EventRepository();
            orderRepository = new OrderRepository();
            ticketRepository = new TicketRepository();
            userRepository = new UserRepository();
            venueRepository = new VenueRepository();
            passwordRepository = new PasswordReposirory();
            roleRepository = new RoleRepository();
            cityRepository.Cities.Add(new City { ID = 1, Name = "Gomel" });
            cityRepository.Cities.Add(new City { ID = 2, Name = "Minsk" });
            cityRepository.Cities.Add(new City { ID = 3, Name = "Grodno" });
            cityRepository.Cities.Add(new City { ID = 4, Name = "Brest" });
            cityRepository.Cities.Add(new City { ID = 4, Name = "Vitebsk" });
            cityRepository.Cities.Add(new City { ID = 4, Name = "Mogilev" });
            eventRepository.Events.Add(new Event { ID = 1, Date = new DateTime(2017, 12, 1), Description = "SomeText", Name = "Concert", VenueId = 1, CityID=1, Banner= "images/1.jpg" });
            eventRepository.Events.Add(new Event { ID = 2, Date = new DateTime(2017, 12, 2), Description = "SomeText", Name = "Concert", VenueId = 2, CityID = 1, Banner = "images/2.jpg" });
            eventRepository.Events.Add(new Event { ID = 3, Date = new DateTime(2017, 12, 3), Description = "SomeText", Name = "Concert", VenueId = 3, CityID = 2, Banner = "images/3.jpg" });
            eventRepository.Events.Add(new Event { ID = 4, Date = new DateTime(2017, 12, 4), Description = "SomeText", Name = "Concert", VenueId = 4, CityID = 2, Banner = "images/4.jpg" });
            venueRepository.Venues.Add(new Venue { ID = 1, CityID = 1, Name = "PalaceOfCulture", Address = "Lenina 1" });
            venueRepository.Venues.Add(new Venue { ID = 2, CityID = 1, Name = "CityCultureCenter", Address = "Irininskaya 35" });
            venueRepository.Venues.Add(new Venue { ID = 3, CityID = 2, Name = "PalaceOfCulture", Address = "Bogdanovicha 41" });
            venueRepository.Venues.Add(new Venue { ID = 4, CityID = 2, Name = "CityCultureCenter", Address = "Kolasa 46" });
            venueRepository.Venues.Add(new Venue { ID = 5, CityID = 3, Name = "PalaceOfCulture", Address = "Rabkorovskaya 34" });
            venueRepository.Venues.Add(new Venue { ID = 6, CityID = 3, Name = "CityCultureCenter", Address = "Sovetskaya 4" });
            venueRepository.Venues.Add(new Venue { ID = 7, CityID = 4, Name = "PalaceOfCulture", Address = "Lange 3" });
            venueRepository.Venues.Add(new Venue { ID = 8, CityID = 4, Name = "CityCultureCenter", Address = "Volgogradskya 23" });
            venueRepository.Venues.Add(new Venue { ID = 9, CityID = 5, Name = "PalaceOfCulture", Address = "Karpovicha 4" });
            venueRepository.Venues.Add(new Venue { ID = 10, CityID = 5, Name = "CityCultureCenter", Address = "Tvena 56" });
            venueRepository.Venues.Add(new Venue { ID = 11, CityID = 6, Name = "PalaceOfCulture", Address = "Ryabinovaya 149" });
            venueRepository.Venues.Add(new Venue { ID = 12, CityID = 6, Name = "CityCultureCenter", Address = "Belyaeva 48" });
            userRepository.Users.Add(new User { ID = 1, Nick="Admin", FirstName = "AdminF", LastName = "AdminL", PhoneNumber = "23-23-23", Address = "Gomel, Oskina 1", Localization = "ru", PasswordID=1, RoleID=1 });
            userRepository.Users.Add(new User { ID = 2, Nick="User", FirstName = "UserF", LastName = "UserL", Localization = "ru", Address = "Gomel, Oskina 1", PhoneNumber = "23-23-23", PasswordID = 2, RoleID=2 });
            passwordRepository.Passwords.Add(new Password { ID = 1, Password_ = "AKQHgxN2kYQL6dRQrt4hrdrWLyIx/+EIKTvMJNc1bxxh6zku05YwCVqG52JN0fADUQ==" });
            passwordRepository.Passwords.Add(new Password { ID = 2, Password_ = "AF0mgIfAwXlIj0hMguBwLlzojfV9ouWs84DAeBHo4Lo1hEggYf7Y3DbRGD42m1QRJA==" });
            roleRepository.Roles.Add(new Role { ID = 1, Name = "Admin" });
            roleRepository.Roles.Add(new Role { ID = 2, Name = "User" });
            roleRepository.Roles.Add(new Role { ID = 3, Name = "Guest" });
            orderRepository.Orders.Add(new Order { ID = 1, BuyerID = 1, Status = false, TicketID = 1, TrackNumber = "RA123456789RU" });
            orderRepository.Orders.Add(new Order { ID = 2, BuyerID = 2, Status = true, TicketID = 2, TrackNumber = "RA123456789RU" });
            orderRepository.Orders.Add(new Order { ID = 3, BuyerID = 1, Status = false, TicketID = 3, TrackNumber = "RA123456789RU" });
            orderRepository.Orders.Add(new Order { ID = 4, BuyerID = 2, Status = false, TicketID = 4, TrackNumber = "RA123456789RU" });
            orderRepository.Orders.Add(new Order { ID = 5, BuyerID = 2, Status =true, TicketID = 5, TrackNumber = "RA123456789RU" });
            orderRepository.Orders.Add(new Order { ID = 6, BuyerID = 1, Status = false, TicketID = 6, TrackNumber = "RA123456789RU" });
            orderRepository.Orders.Add(new Order { ID = 7, BuyerID = 1, Status = false, TicketID = 7, TrackNumber = "RA123456789RU" });
            orderRepository.Orders.Add(new Order { ID = 8, BuyerID = 1, Status = false, TicketID = 8, TrackNumber = "RA123456789RU" });
            orderRepository.Orders.Add(new Order { ID = 9, BuyerID = 2, Status = true, TicketID = 9, TrackNumber = "RA123456789RU" });
            orderRepository.Orders.Add(new Order { ID = 10, BuyerID = 1, Status = false, TicketID = 12, TrackNumber = "RA123456789RU" });

            Random a = new Random();
            int[] prices =new int[5] { 100, 200, 300, 400, 500 };
            for(int i=1; i<=100; i++)
            {
                ticketRepository.Tickets.Add(new Ticket { ID = i, EventID = a.Next(1, 4), Price = prices[a.Next(0, 5)], SellerID=1, Availability=true });
            }
        }

        public IRepository <Event> Events
        {
            get
            {
                if (eventRepository == null)
                    eventRepository = new EventRepository();
                return eventRepository;
            }
        }

        public IRepository<Role> Roles
        {
            get
            {
                if (roleRepository == null)
                    roleRepository = new RoleRepository();
                return roleRepository;
            }
        }

        public IRepository<Password> Passwords
        {
            get
            {
                if (passwordRepository == null)
                {
                    passwordRepository = new PasswordReposirory();
                }
                return passwordRepository;
            }
        }

        public IRepository<Venue> Venues
        {
            get
            {
                if (venueRepository == null)
                    venueRepository = new VenueRepository();
                return venueRepository;
            }
        }

        public IRepository<User> Users
        {
            get
            {
                if (userRepository == null)
                    userRepository = new UserRepository();
                return userRepository;
            }
        }

        public IRepository<Ticket> Tickets
        {
            get
            {
                if (ticketRepository == null)
                    ticketRepository = new TicketRepository();
                return ticketRepository;
            }
        }

        public IRepository<City> Cities
        {
            get
            {
                if (cityRepository == null)
                    cityRepository = new CityRepository();
                return cityRepository;
            }
        }

        public IRepository<Order> Orders
        {
            get
            {
                if (orderRepository == null)
                    orderRepository = new OrderRepository();
                return orderRepository;
            }
        }
    }
}
