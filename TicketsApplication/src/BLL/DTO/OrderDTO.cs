﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BLL.DTO
{
    public class OrderDTO
    {
        public int ID { get; set; }
        public int TicketID { get; set; }
        public bool Status { get; set; }
        public int BuyerID { get; set; }
        public string TrackNumber { get; set; }
    }
}
