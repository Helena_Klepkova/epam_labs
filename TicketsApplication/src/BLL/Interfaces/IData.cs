﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DAL.Interfaces;
using DAL.Entities;

namespace BLL.Interfaces
{
    public interface IData
    {
        IRepository<City> Cities { get; }
        IRepository<Event> Events { get; }
        IRepository<Order> Orders { get; }
        IRepository<Ticket> Tickets { get; }
        IRepository<User> Users { get; }
        IRepository<Venue> Venues { get; }
        IRepository<Password> Passwords { get; }
        IRepository<Role> Roles { get; }
    }
}
