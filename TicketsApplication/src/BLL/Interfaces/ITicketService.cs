﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BLL.DTO;

namespace BLL.Interfaces
{
    public interface ITicketService
    {
        ICollection<KeyValuePair<decimal, int>> GetTicketCountDTO(int eventId);
        EventDTO GetEventForTicket(int eventId);
        List<TicketDTO> TicketsForEvent(int eventID, decimal price);
        List<TicketDTO> TicketsForUser(string nick);
    }
}
