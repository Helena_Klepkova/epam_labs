﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BLL.DTO;

namespace BLL.Interfaces
{
   public interface IUserInfo
    {
        UserDTO GetUser(string nick);
    }
}
