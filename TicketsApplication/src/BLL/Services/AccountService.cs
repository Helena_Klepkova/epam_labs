﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BLL.Interfaces;
using BLL.DTO;
using AutoMapper;
using DAL.Entities;

namespace BLL.Services
{
    public class AccountService : IAccount
    {
        IData data;
        public AccountService(IData data)
        {
            this.data = data;
        }

        private List<UserDTO> Users()=> Mapper.Map<List<UserDTO>>(data.Users.GetAll());
        private List<PasswordDTO> Passwords()=> Mapper.Map<List<PasswordDTO>>(data.Passwords.GetAll());

        public int GetPasswordID(string password) => Passwords().Find(o => o.Password_ == password).ID;

        public int GetPasswordIdInUsers(string nick)=>((List<User>)data.Users.GetAll()).Find(o => o.Nick == nick).PasswordID;
     
      //  public int GetPasswordIdInUsers(string nick) => Users().Find(o => o.Nick == nick).PasswordID;

        public string GetPassword(int id) => data.Passwords.Get(id).Password_;

        public int NewPasswordID()=> (Passwords().Count() + 1);

        public bool IsUser(string name)=> Users().Exists(u => u.FirstName == name);

        public UserDTO GetUser(string nick, int passwordID)
        {
                List<UserDTO> users = Mapper.Map<List<UserDTO>>(data.Users.GetAll());
                UserDTO user = users.Find(o => (o.Nick == nick)&&(o.PasswordID==passwordID));           
                return user;        
        }

        public bool Register(UserDTO user, string password)
        {
            if (!IsUser(user.Nick))
            {
                User userForReg = Mapper.Map<User>(user);
                data.Users.Create(userForReg);
                Password psw = new Password { Password_ = password };
                data.Passwords.Create(psw);
                return true;
            }
            return false;
        }

        public UserDTO CreateUser(string nick, string firstName, string lastName, int passwordId, 
                                  string address, string localization, string phone)
        {
            UserDTO user= new UserDTO();
            user.Nick = nick;
            user.FirstName = firstName;
            user.LastName = lastName;
            user.PasswordID = passwordId;
            user.Address = address;
            user.PhoneNumber = phone;
            user.Localization = localization;
            return user;
               // user.Password = Crypto.HashPassword(password);
                //user.roleID = (int)MvcLab7.Models.Roles.AllRoles.user;
              // _db.Users.Add(user);
                //_db.SaveChanges();
               // membershipUser = GetUser(username, false);                  
      }
    }
}
