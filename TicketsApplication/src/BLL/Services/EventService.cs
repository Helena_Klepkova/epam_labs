﻿using System;
using System.Collections.Generic;
using BLL.Interfaces;
using BLL.DTO;
using DAL.Entities;
using AutoMapper;

namespace BLL.Services
{
    public class EventService : IEventService
    {
        IData data;
        public EventService(IData data)
        {
            this.data = data;
        }

        public IEnumerable<EventDTO> GetEventsWithCity()
        {
            IEnumerable<EventDTO> collection = GetAllEvents();
            List<Venue> venues = data.Venues.GetAll();
            List<City> cities = data.Cities.GetAll();
            foreach (var i in collection)
            {
                i.Venue = venues.Find(o => o.ID == i.VenueId).Name;
                i.City = cities.Find(o => o.ID == i.CityID).Name;
            }
            return collection;
        }

        private List<EventDTO> GetAllEvents()
        {
            return Mapper.Map<List<EventDTO>>(data.Events.GetAll());
        }
    }
}
