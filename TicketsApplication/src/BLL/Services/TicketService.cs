﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BLL.Interfaces;
using BLL.DTO;
using AutoMapper;
using DAL.Entities;

namespace BLL.Services
{
    public class TicketService : ITicketService
    {
        IData Data;
        List<TicketDTO> TicketsCopy;

        public TicketService(IData data)
        {
            Data = data;
        }

        private List<TicketDTO> Tickets()=> Mapper.Map<List<TicketDTO>>(Data.Tickets.GetAll());

        private List<TicketDTO> TicketsForEvent(int eventID) => (Tickets()
            .FindAll(o => (o.EventID == eventID) && (o.Availability)));

        private List<TicketDTO> TicketsForEventWithoutSeller(int eventID, decimal price) => (Tickets()
            .FindAll(o => (o.EventID == eventID) && (o.Availability) && (o.Price == price)));


        private ICollection<KeyValuePair<decimal, int>> AddCountWithPrice(int eventId)
        {
            TicketsCopy = TicketsForEvent(eventId);
            ICollection<KeyValuePair<decimal, int>> priceAndCount =
          new Dictionary<decimal, int>();
            while (TicketsCopy.Count > 0)
            {
                decimal price = TicketsCopy[0].Price;
                int count = TicketsCopy.FindAll(o => o.Price == price).Count();
                priceAndCount.Add(new KeyValuePair<decimal, int>(price, count));
                TicketsCopy.RemoveAll(o => o.Price == price);
            }
            return priceAndCount;
        }
        public List<TicketDTO> TicketsForUser(string nick)
        {
            int userId = ((List<User>)Data.Users.GetAll()).Find(o => o.Nick==nick).ID;
            List<Order> ordersUser = Data.Orders.GetAll().FindAll(o => o.BuyerID == userId);
            List<User> users = Data.Users.GetAll();
            var allTickets = Tickets();
            List<TicketDTO> tickets = new List<TicketDTO>();
            foreach(var i in ordersUser)
            {
                int ti = i.TicketID;
                tickets.Add(allTickets.Find(o => o.ID == ti));
            }
            List<Event> events = Data.Events.GetAll();
            foreach(var i in tickets)
            {
                i.Event = events.Find(o => o.ID == i.EventID).Name;
                i.Seller = users.Find(o => o.ID == i.SellerID).Nick;
            }
            return tickets;
        }

        public ICollection<KeyValuePair<decimal, int>> GetTicketCountDTO(int eventId)
        {
            TicketCountDTO tcd = new TicketCountDTO();
            tcd.PriceAndCount = AddCountWithPrice(eventId);
            return (tcd.PriceAndCount);
        }

        public EventDTO GetEventForTicket(int eventId)
        {
            Event ev = Data.Events.GetAll().Find(o => o.ID == eventId);
            EventDTO e = Mapper.Map<EventDTO>(ev);
            e.City = Data.Cities.GetAll().Find(o => o.ID == e.CityID).Name;
            e.Venue = Data.Venues.GetAll().Find(o => o.ID == e.VenueId).Name;
            return e;
        }

        public List<TicketDTO> TicketsForEvent(int eventID, decimal price)
        {
            List<TicketDTO> tickets = TicketsForEventWithoutSeller(eventID, price);
            List<User> users = Data.Users.GetAll();
            foreach (var i in tickets)
            {
                i.Seller = users.Find(o => o.ID == i.SellerID).FirstName;
            }
            return tickets;
        }

        public TicketDTO GetTicket(int id)
        {
            return (Tickets().Find(o => o.ID == id));
        }
    }
}
