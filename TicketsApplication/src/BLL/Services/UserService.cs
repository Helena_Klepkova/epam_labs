﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BLL.Interfaces;
using BLL.DTO;
using DAL.Entities;
using AutoMapper;

namespace BLL.Services
{
    public class UserService : IUserInfo
    {
        IData data;
        public UserService(IData data)
        {
            this.data = data;
        }
        public UserDTO GetUser(string nick)
        {
            List<User> users = data.Users.GetAll();
            return Mapper.Map<UserDTO>(users.Find(o => o.Nick == nick));
        }
    }
}
