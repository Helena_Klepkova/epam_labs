﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DAL.Entities
{
    public class Order
    {
        public int ID { get; set; }
        public int TicketID { get; set; }
        public bool Status { get; set; }
        public int BuyerID { get; set; }
        public string TrackNumber { get; set; }
    }
}

