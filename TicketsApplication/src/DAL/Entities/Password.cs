﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DAL.Entities
{
    public class Password
    {
        public int ID { get; set; }
        public int UserID { get; set; }
        public string Password_ { get; set; }
    }
}
