﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DAL.Entities
{
    public class Ticket
    {
        public int ID { get; set; }
        public int EventID { get; set; }
        public decimal Price { get; set; }
        public int SellerID { get; set; }
        public bool Availability { get; set; }
    }
}
