﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DAL.Entities;
using DAL.Interfaces;

namespace DAL.Repositories
{
    public class CityRepository : IRepository<City>
    {
        public List<City> Cities = new List<City>();
        public List<City> GetAll()
        {
            return Cities;
        }

        private int GetIDForAdding()
        {
            return (Cities.Count()+1);
        }

        public City Get(int id)
        {
            return Cities.Find(o => o.ID == id);
        }

        public void Create(City city)
        {
            city.ID = GetIDForAdding();
            Cities.Add(city);
        }

        public void Update(City city)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            City ct = Cities.Find(o => o.ID == id);
            if (ct != null)
            {
                Cities.Remove(ct);
            }
        }
    }
}
