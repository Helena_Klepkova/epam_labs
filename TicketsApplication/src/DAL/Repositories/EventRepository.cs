﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DAL.Entities;
using DAL.Interfaces;

namespace DAL.Repositories
{
    public class EventRepository : IRepository<Event>
    {
        public List<Event> Events = new List<Event>();
        public List<Event> GetAll()
        {
            return Events;
        }

        private int GetIDForAdding()
        {
            return (Events.Count()+1);
        }
        public IEnumerable<Event> GetByCity(int cityID)
        {
            return Events.FindAll(o => o.CityID == cityID);
        }

        public Event Get(int id)
        {
            return Events.Find(o => o.ID == id);
        }

        public void Create(Event _event)
        {
            _event.ID = GetIDForAdding();
            Events.Add(_event);
        }

        public void Update(Event _event)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            Event ev = Events.Find(o => o.ID == id);
            if (ev != null)
            {
                Events.Remove(ev);
            }
        }
    }
}
