﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DAL.Entities;
using DAL.Interfaces;

namespace DAL.Repositories
{
    public class OrderRepository : IRepository<Order>
    {
        public List<Order> Orders = new List<Order>();
        public List<Order> GetAll()
        {
            return Orders;
        }

        private int GetIDForAdding()
        {
            return (Orders.Count()+1);
        }

        public Order Get(int id)
        {
            return Orders.Find(o => o.ID == id);
        }

        public void Create(Order order)
        {
            order.ID = GetIDForAdding();
            Orders.Add(order);
        }

        public void Update(Order order)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            Order or = Orders.Find(o => o.ID == id);
            if (or != null)
            {
                Orders.Remove(or);
            }
        }
    }
}
