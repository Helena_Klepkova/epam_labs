﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Threading.Tasks;
using DAL.Entities;
using DAL.Interfaces;
using DAL.Hash;


namespace DAL.Repositories
{
    public class PasswordReposirory : IRepository<Password>
    {
        public List<Password> Passwords = new List<Password>();
        public List<Password> GetAll()
        {
            return Passwords;
        }

        private int GetIDForAdding()
        {
            return (Passwords.Count()+1);
        }

        public Password Get(int id)
        {
            return Passwords.Find(o => o.ID == id);
        }
        

        public void Create(Password password)
        {       
            password.ID = GetIDForAdding();
            password.Password_ = Hash.Hash.HashPassword(password.Password_);
            Passwords.Add(password);
        }

        public void Update(Password password)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
           Password pw = Passwords.Find(o => o.ID == id);
            if (pw != null)
            {
                Passwords.Remove(pw);
            }
        }
    }
}
