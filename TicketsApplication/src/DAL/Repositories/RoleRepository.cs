﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DAL.Entities;
using DAL.Interfaces;

namespace DAL.Repositories
{
    public class RoleRepository : IRepository<Role>
    {
        public List<Role> Roles = new List<Role>();

        public List<Role> GetAll()
        {
            return Roles;
        }

        private int GetIDForAdding()
        {
            return (Roles.Count() + 1);
        }

        public Role Get(int id)
        {
            return Roles.Find(o => o.ID == id);
        }

        public void Create(Role role)
        {
            role.ID = GetIDForAdding();
            Roles.Add(role);
        }

        public void Update(Role role)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            Role rl = Roles.Find(o => o.ID == id);
            if (rl != null)
            {
                Roles.Remove(rl);
            }
        }
    }
}
