﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DAL.Entities;
using DAL.Interfaces;

namespace DAL.Repositories
{
    public class TicketRepository : IRepository<Ticket>
    {
        public List<Ticket> Tickets = new List<Ticket>();
        public List<Ticket> GetAll()
        {
            return Tickets;
        }

        private int GetIDForAdding()
        {
            return (Tickets.Count()+1);
        }

        public Ticket Get(int id)
        {
            return Tickets.Find(o => o.ID == id);
        }

        public void Create(Ticket ticket)
        {
            ticket.ID = GetIDForAdding();
            Tickets.Add(ticket);
        }

        public void Update(Ticket ticket)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            Ticket tk = Tickets.Find(o => o.ID == id);
            if (tk != null)
            {
                Tickets.Remove(tk);
            }
        }
    }
}
