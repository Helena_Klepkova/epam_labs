﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DAL.Entities;
using DAL.Interfaces;

namespace DAL.Repositories
{
    public class UserRepository : IRepository<User>
    {
        public List<User> Users = new List<User>();
        public List<User> GetAll()
        {
            return Users;
        }

        private int GetIDForAdding()
        {
            return (Users.Count()+1);
        }

        public User Get(int id)
        {
            return Users.Find(o => o.ID == id);
        }

        public void Create(User user)
        {
            user.ID = GetIDForAdding();
            Users.Add(user);
        }

        public void Update(User user)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            User us = Users.Find(o => o.ID == id);
            if (us != null)
            {
                Users.Remove(us);
            }
        }
    }
}
