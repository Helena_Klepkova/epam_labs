﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DAL.Entities;
using DAL.Interfaces;

namespace DAL.Repositories
{
    public class VenueRepository : IRepository<Venue>
    {
        public List<Venue> Venues = new List<Venue>();
        public List<Venue> GetAll()
        {
            return Venues;
        }

        private int GetIDForAdding()
        {
            return (Venues.Count()+1);
        }

        public Venue Get(int id)
        {
            return Venues.Find(o => o.ID == id);
        }

        public void Create(Venue venue)
        {
            venue.ID = GetIDForAdding();
            Venues.Add(venue);
        }

        public void Update(Venue venue)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            Venue vn = Venues.Find(o => o.ID == id);
            if (vn != null)
            {
                Venues.Remove(vn);
            }
        }
    }
}
