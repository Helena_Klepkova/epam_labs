﻿using System;
using System.Collections.Generic;
using BLL.Interfaces;
using Microsoft.AspNetCore.Mvc;
using TicketsApplication.Models;
using BLL.DTO;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;


namespace TicketsApplication.Controllers
{
    public class HomeController : Controller
    {
        IEventService eventService;
        public HomeController(IEventService serv)
        {
            eventService = serv;
        }

        private List<EventVM> Events()
        {
            IEnumerable<EventDTO> eventDtos = eventService.GetEventsWithCity();
            return Mapper.Map<List<EventVM>>(eventDtos);
        }

        public IActionResult Index()
        {
            if (HttpContext.User.Identity.IsAuthenticated)
            {
                HttpContext.Session.SetString("User", HttpContext.User.Identity.Name);
                ViewBag.Message = HttpContext.Session.GetString("User");
            }
            else
            {
                ViewBag.Message = "Guest";
            }
            
            return View(Events());
        }
    }
}
