﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using BLL.Interfaces;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using TicketsApplication.Models;
using BLL.DTO;
using Microsoft.AspNetCore.Authorization;

namespace TicketsApplication.Controllers
{
    [Authorize]
    public class TicketController : Controller
    {
        ITicketService ts;

        public TicketController(ITicketService ts)
        {
            this.ts = ts;
        }

        private void UserTitle()
        {
            if (HttpContext.User.Identity.IsAuthenticated)
            {
                HttpContext.Session.SetString("User", HttpContext.User.Identity.Name);
                ViewBag.Message = HttpContext.Session.GetString("User");
            }
            else
            {
                ViewBag.Message = "Guest";
            }
        }

        [AllowAnonymous]
        public IActionResult Tickets(int id)
        {
            UserTitle();
            TicketCountVM tcvm = new TicketCountVM();
            tcvm.PriceAndCount = Mapper.Map<ICollection<KeyValuePair<decimal, int>>>(ts.GetTicketCountDTO(id));
            EventDTO ev = ts.GetEventForTicket(id);
            tcvm.Event = Mapper.Map<EventVM>(ev);
            return View(tcvm);
        }

        public IActionResult TicketsForOrder(decimal price, int eventId)
        {
            UserTitle();
            List<TicketDTO> tdto = ts.TicketsForEvent(eventId, price);
            List<TicketVM> tickets = Mapper.Map<List<TicketVM>>(tdto);
            return View(tickets);
        }

        public IActionResult UserTickets(string nick)
        {
            UserTitle();
            List<TicketVM> tickets = Mapper.Map<List<TicketVM>>(ts.TicketsForUser(nick));
            return View(tickets);
        }
        public IActionResult Buy(List<TicketVM> models)
        {
            return View();
        }
    }
}
