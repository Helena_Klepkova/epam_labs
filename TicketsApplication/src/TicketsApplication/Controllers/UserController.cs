﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using BLL.Interfaces;
using TicketsApplication.Models;
using AutoMapper;
using Microsoft.AspNetCore.Http;

namespace TicketsApplication.Controllers
{
    public class UserController : Controller
    {
        IUserInfo us;
        public UserController(IUserInfo us)
        {
            this.us = us;
        }
        private void UserTitle()
        {
            if (HttpContext.User.Identity.IsAuthenticated)
            {
                HttpContext.Session.SetString("User", HttpContext.User.Identity.Name);
                ViewBag.Message = HttpContext.Session.GetString("User");
            }
            else
            {
                ViewBag.Message = "Guest";
            }
        }

        public IActionResult UserInformation(string nick)
        {
            UserTitle();
            UserVM user = Mapper.Map<UserVM>(us.GetUser(nick));
            return View(user);
        }
    }
}
