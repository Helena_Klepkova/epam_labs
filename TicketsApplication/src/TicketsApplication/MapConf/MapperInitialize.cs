﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using BLL.DTO;
using DAL.Entities;

namespace TicketsApplication.MapConf
{
    public class MapperInitialize : Profile
    { 
        public MapperInitialize()
        {
            CreateMap<Password, PasswordDTO>();
            CreateMap<PasswordDTO, Password>();

            CreateMap<EventDTO, Event>();
            CreateMap<Event, EventDTO>();

            CreateMap<UserDTO, User>();
            CreateMap<User, UserDTO>();

            CreateMap<CityDTO, City>();
            CreateMap<City, CityDTO>();

            CreateMap<OrderDTO, Order>();
            CreateMap<Order, OrderDTO>();

            CreateMap<TicketDTO, Ticket>();
            CreateMap<Ticket, TicketDTO>();

            CreateMap<VenueDTO, Venue>();
            CreateMap<Venue, VenueDTO>();
        }
    }
}
