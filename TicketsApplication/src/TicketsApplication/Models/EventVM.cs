﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TicketsApplication.Models
{
    public class EventVM
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public DateTime Date { get; set; }
        public int VenueID { get; set; }
        public string Banner { get; set; }
        public string Description { get; set; }
        public int CityID { get; set; }
        public string Venue { get; set; }
        public string City { get; set; }
    }
}
