﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TicketsApplication.Models
{
    public class LoginModel
    {
        [Required(ErrorMessage = "Unknown Nick")]
        public string Nick { get; set; }

        [Required(ErrorMessage = "Unknown Password")]
        [DataType(DataType.Password)]
        public string Password { get; set; }
    }
}
