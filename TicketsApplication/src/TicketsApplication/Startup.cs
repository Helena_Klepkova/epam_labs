﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using BLL.Interfaces;
using BLL.AppData;
using AutoMapper;
using TicketsApplication.MapConf;
using BLL.DTO;
using TicketsApplication.Models;
using BLL.Services;


namespace TicketsApplication
{
    public class Startup
    {
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();
            services.AddSingleton<IData, Data>();
            services.AddTransient<IEventService, EventService>();
            services.AddTransient<IAccount, AccountService>();
            services.AddTransient<ITicketService, TicketService>();
            services.AddTransient<IUserInfo, UserService>();
            services.AddDistributedMemoryCache();
            services.AddSession();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            Mapper.Initialize(c =>
            {
                c.CreateMap<TicketCountDTO, TicketCountVM>();
                c.CreateMap<TicketCountVM, TicketCountDTO>();

                c.CreateMap<CityVM, CityDTO>();
                c.CreateMap<CityDTO, CityVM>();

                c.CreateMap<EventVM, EventDTO>();
                c.CreateMap<EventDTO, EventVM>();

                c.CreateMap<TicketVM, TicketDTO>();
                c.CreateMap<TicketDTO, TicketVM>();

                c.CreateMap<UserVM, UserDTO>();
                c.CreateMap<UserDTO, UserVM>();

                c.AddProfile<MapperInitialize>();
            });
            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {

                AuthenticationScheme = "Cookies",
                LoginPath = new Microsoft.AspNetCore.Http.PathString("/Account/Login"),
                AutomaticAuthenticate = true,
                AutomaticChallenge = true
            });
            app.UseStaticFiles();
            app.UseSession();
            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");

            });
        }
    }
}
